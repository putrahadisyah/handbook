# Wishlist

## Apa Ini

Dokumen ephemeral yang berisi fitur-fitur tambahan di luar kebutuhan utama website kawalcovid19 sesuai dokumen [Overview](./overview.md). Ephemeral karena baiknya wishlist dipindahkan ke issue tracker bukan di sini :))

## The Wishlist

    - Broadcast WhatsApp atau semacamnya e.g. ala go.gov.sg/whatsapp
    - Public submissions
    Warganet mampu meminta langsung ke tim kawalcovid19 via website verifikasi terhadap suatu isu atau informasi lain yang belum diulas di Website Utama
